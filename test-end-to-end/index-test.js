var webdriver = require('selenium-webdriver');
var firefox = require('selenium-webdriver/firefox');
var expect = require('chai').expect;
var By = webdriver.By;
var Key = webdriver.Key;

var options = new firefox.Options();
options.addArguments('-headless');

var driver = new webdriver.Builder()
  .forBrowser('firefox')
  .setFirefoxOptions(options)
  .build();

describe('Acceptance tests', () => {
  beforeEach(() => {
    driver.manage().setTimeouts({implicit: 15000});
    driver.get('http://localhost:8080');
  });

  after(() => {
    driver.quit();
  });
  
  it('should be possible to add text to active buffer', (done) => {
    driver.findElement(By.id('chisel-active-buffer')).then((elem) => {
      elem.findElements(By.className('editable-area')).then(function(elem) {
        elem = elem[0];
        elem.sendKeys('Writing to buffer');
        driver.sleep(10).then(() => {
          elem.getText().then((bufferText) => {
            expect(bufferText).to.equal('Writing to buffer');
            done();
          });
        });
      });
    });
  });

  it('should be possible to make a heading', (done) => {
    driver.findElement(By.id('chisel-active-buffer')).then((elem) => {
      elem.findElements(By.className('editable-area')).then(function(elem) {
        elem = elem[0];
        elem.sendKeys('This is a heading', Key.chord(Key.CONTROL, "2"));
        driver.sleep(10).then(() => {
          elem.findElements(By.tagName('h2')).then((elem) => {
            elem[0].getText().then((text) => {
              expect(text).to.equal('This is a heading');
              done();
            }).catch((err) => {
              console.log(err);
              done();
            });
          });
        });
      });
    });
  });
});
