import TestComponent from './components/test-component';
import Component from '../src/framework/component';

describe('Component tests', () => {
  it('template should return rendered html', () => {
    const component = new TestComponent({id: 'component-id-test'});
    document.getElementsByTagName('body')[0].innerHTML = component.render();
    document.getElementById('component-id-test').should.exist;
  });

  it('should not add anything to dom if base class used directly', () => {
    const newElem = document.createElement('div');
    document.getElementsByTagName('body')[0].appendChild(newElem);

    const component = new Component({});
    newElem.innerHTML = component.render();

    newElem.innerHTML.should.be.empty;
  });
});
