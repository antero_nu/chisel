import Component from '../../src/framework/component';
import template from './test-component-template.html';


export default class TestComponent extends Component {
  constructor(options) {
    super(options);
    this.template = template;
  }
}
