import Chisel from '../../src/components/chisel';
import TestComponent from './test-component';

describe('Chisel', () => {
  it('should add its children', () => {
    const chisel = new Chisel();
    chisel.addChild(new TestComponent({id: 'this-component-id'}));
    const result = chisel.render();
    result.should.contain('this-component-id')
  })
});