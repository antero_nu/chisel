import Toolbar from '../../src/components/toolbar/toolbar';

describe('Toolbar', () => {
  it('should have a list to choose heading and paragraph', () => {
    const toolbar = new Toolbar();
    const toolbarHTML = toolbar.render();
    toolbarHTML.should.contain('<select id="chisel-heading-picker">');
  });
});