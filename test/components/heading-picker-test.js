import HeadingPicker from '../../src/components/heading-picker/heading-picker';

describe('heading-picker', () => {
  let headingPickerHTML = null;

  beforeEach(() => {
    const headingPicker = new HeadingPicker();
    headingPickerHTML = headingPicker.render();
  });

  it('should be possible to make a paragraph', () => {
    headingPickerHTML.should.contain('<option value="p">paragraph</option>');
  });

  const headingText = {
    h1: 'Heading 1',
    h2: 'Heading 2',
    h3: 'Heading 3',
    h4: 'Heading 4',
    h5: 'Heading 5',
    h6: 'Heading 6'
  };

  ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'].forEach((heading) => {
    it('should be possible to make ' + headingText[heading], () => {
      let optionText = '<option value="' +
        heading + '">' + headingText[heading] + '</option>';
      headingPickerHTML.should.contain(optionText);
    });
  });
});