export default class Component {
  constructor(options = {}) {
    this.options = options;
    this.options.children = '';
    this.children = [];
    this.template = () => { return ''; };
    this.actions = () => {};
  }

  addChild(child) {
    if (child.render) {
      this.children.push(child);
    }
  }

  render() {
    for (let i = 0; i < this.children.length; i++) {
      this.options.children += this.children[i].render();
    }

    return this.template(this.options);
  }

  registerActions() {
    this.actions();

    for (let i = 0; i < this.children.length; i++) {
      this.children[i].registerActions();
    }
  }
}