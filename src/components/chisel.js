import Component from '../framework/component';
import template from './chisel.html';
import './chisel.css';

export default class Chisel extends Component {
  constructor() {
    super();
    this.template = template;
  }
}
