import Component from '../../framework/component';
import actions from './heading-picker-actions';
import template from './heading-picker.html';
import './heading-picker.css';

export default class HeadingPicker extends Component {
  constructor() {
    super();
    this.actions = actions;
    this.template = template;
  }
}