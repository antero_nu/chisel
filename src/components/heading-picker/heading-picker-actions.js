export default function() {
  const picker = document.getElementById('chisel-heading-picker');
  picker.addEventListener('change', (event) => {
    const buffer = document.querySelector('#chisel-active-buffer .editable-area');
    let caretNode = document.getSelection().anchorNode;

    while (caretNode && !options.includes(caretNode.tagName)) {
      caretNode = caretNode.parentNode;
    }

    if (caretNode) {
      const html = caretNode.innerHTML;
      const newHeading = document.createElement(event.target.value);
      newHeading.innerHTML = html;
      caretNode.parentNode.replaceChild(newHeading, caretNode);
    }

    buffer.focus();
  });
};

const options = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'P'];