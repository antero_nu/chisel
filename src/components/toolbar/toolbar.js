import Component from '../../framework/component';
import HeadingPicker from '../heading-picker/heading-picker';
import template from './toolbar.html';
import './toolbar.css';

export default class Toolbar extends Component {
  constructor() {
    super();
    this.template = template;
    this.initializeChildren();
  }

  initializeChildren() {
    this.addChild(new HeadingPicker());
  }
}