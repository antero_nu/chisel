import Component from '../../framework/component';
import template from './active-buffer.html';
import './active-buffer.css';

export default class ActiveBuffer extends Component {
  constructor(parent) {
    super(parent);
    this.template = template;
  }

}