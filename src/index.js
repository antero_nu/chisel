import 'babel-polyfill';
import Chisel from './components/chisel';
import Toolbar from './components/toolbar/toolbar';
import ActiveBuffer from './components/active-buffer/active-buffer';
import './styles.css';

const chisel = new Chisel();
const toolbar = new Toolbar();
chisel.addChild(toolbar);

const buffer = new ActiveBuffer();
chisel.addChild(buffer);

const body = document.getElementsByTagName('body')[0];
body.innerHTML = chisel.render();
chisel.registerActions();